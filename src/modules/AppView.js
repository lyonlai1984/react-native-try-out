import React from 'react';
import {fromJS} from 'immutable';
import {View} from 'react-native';
import DeveloperMenu from '../components/DeveloperMenu';
import MenuNav from './menus/MenuViewContainer';

const AppView = React.createClass({
  componentDidMount() {
    fetch('http://dock:5984/organisation-55/_all_docs?include_docs=true')
      .then(response => {
        const result = JSON.parse(response._bodyText);
        const appState = result.rows.reduce((acc, row) => {
          const id = row.id;
          const [type, docId] = id.split('/');

          if (type === '_design') {
            return acc;
          }

          const doc = fromJS(row.doc[type]);

          return acc.setIn([type, docId], doc);
        }, fromJS({}));

        console.log(appState.toJS());

        this.props.loadMenus(appState.get('menu'));
      });
  },
  render() {
    return (
      <View style={{flex: 1}}>
        <MenuNav />
        {__DEV__ && <DeveloperMenu />}
      </View>
    );
  }
});

export default AppView;
