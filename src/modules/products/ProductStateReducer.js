import {Map, fromJS} from 'immutable';

// Initial state
const initialState = Map({
  data: Map({})
});

// Actions
const LOAD_PRODUCTS = 'AppState/LOAD_PRODUCTS';

export function loadProducts(products) {
  return {
    type: LOAD_PRODUCTS,
    payload: {
      products: fromJS(products)
    }
  };
}

// Reducer
export default function MenuStateReducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_PRODUCTS:
      return state.set('data', action.payload.products);
    default:
      return state;
  }
}
