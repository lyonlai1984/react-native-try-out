import React, {PropTypes} from 'react';
import {
  View
} from 'react-native';
import Item from './item';

export default React.createClass({
  displayName: 'MenuNav',
  propTypes: {
    menus: PropTypes.object.isRequired,
    currentMenuId: PropTypes.string.isRequired,
    swapMenuList: PropTypes.func.isRequired
  },
  render() {
    const {menus, swapMenuList, currentMenuId} = this.props;
    return (
      <View style={{flex: 1}}>
        {
          menus.map(menu =>
            <Item item={menu}
                  key={menu.get('id')}
                  selected={menu.get('id') === currentMenuId}
                  swapMenuList={swapMenuList} />
          ).toList()
        }
      </View>
    );
  }
});
