import React, {PropTypes} from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

export default React.createClass({
  displayName: 'MenuNavItem',
  propTypes: {
    item: PropTypes.object.isRequired,
    selected: PropTypes.bool.isRequired,
    swapMenuList: PropTypes.func.isRequired
  },
  render() {
    const {item, swapMenuList, selected} = this.props;
    const name = item.get('name');
    const id = item.get('id');

    return (
      <TouchableOpacity
        onPress={() => swapMenuList(id)}
        style={[styles.button, selected && styles.selected]}
        >
        <Text>{name}</Text>
      </TouchableOpacity>
    );
  }
});

const styles = StyleSheet.create({
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  selected: {
    backgroundColor: 'yellow'
  }
});
