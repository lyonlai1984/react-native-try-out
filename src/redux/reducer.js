import {Map} from 'immutable';
import {combineReducers} from 'redux-loop';
import MenuStateReducer from '../modules/menus/MenuStateReducer';
import ProductStateReducer from '../modules/products/ProductStateReducer';

const reducers = {
  // Authentication/login state
  menus: MenuStateReducer,

  // Counter sample app state. This can be removed in a live application
  products: ProductStateReducer

};

// initial state, accessor and mutator for supporting root-level
// immutable data with redux-loop reducer combinator
const immutableStateContainer = Map();
const getImmutable = (child, key) => child ? child.get(key) : void 0;
const setImmutable = (child, key, value) => child.set(key, value);

const namespacedReducer = combineReducers(
  reducers,
  immutableStateContainer,
  getImmutable,
  setImmutable
);

export default function mainReducer(state, action) {
  return namespacedReducer(state || void 0, action);
}
