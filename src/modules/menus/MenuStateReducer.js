import {Map, fromJS} from 'immutable';

// Initial state
const initialState = Map({
  data: fromJS({
    '1': {
      menuLists: {}
    }
  }),
  currentMenuId: ''
});

// Actions
const LOAD_MENUS = 'AppState/LOAD_MENUS';
const UPDATE_CURRENT_MENU = 'AppState/UPDATE_CURRENT_MENU';

export function loadMenus(menus) {
  return {
    type: LOAD_MENUS,
    payload: {
      menus: fromJS(menus)
    }
  };
}

export function setCurrentMenu(menuId) {
  return {
    type: UPDATE_CURRENT_MENU,
    payload: {
      menuId
    }
  };
}

// Reducer
export default function MenuStateReducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_MENUS:
      return state.set('data', action.payload.menus);
    case UPDATE_CURRENT_MENU:
      return state.set('currentMenuId', action.payload.menuId);
    default:
      return state;
  }
}
