import {connect} from 'react-redux';
import {loadMenus} from './menus/MenuStateReducer';
import AppView from './AppView';

export default connect(
  null,
  dispatch => ({
    loadMenus(menus) {
      dispatch(loadMenus(menus))
    }
  })
)(AppView);
