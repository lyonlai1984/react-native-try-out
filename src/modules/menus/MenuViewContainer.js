import {connect} from 'react-redux';
import {setCurrentMenu} from './MenuStateReducer';
import MenuNavView from '../../components/MenuNav/index';

export default connect(
  state => ({
    menus: state.getIn(['menus', 'data', '1', 'menuLists']),
    currentMenuId: state.getIn(['menus', 'currentMenuId'])
  }),
  dispatch => ({
    swapMenuList(menuListId) {
      dispatch(setCurrentMenu(menuListId));
    }
  })
)(MenuNavView);
